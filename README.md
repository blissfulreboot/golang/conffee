# Conffee

Configuration library aiming to make the configuration from several sources possible and easy.

The configuration is simply done as follows:

- Define the configuration as a struct
- Create an instance of it
- Use Conffee to read the values from file (optional), environment (automatic) and CLI arguments

Supports Go tags with `conffee` as the key.