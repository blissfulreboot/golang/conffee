package main

import (
	"fmt"
	"gitlab.com/blissfulreboot/golang/conffee"
)

type exampleSubObject struct {
	SomeSubValue string
}

type exampleConfiguration struct {
	SomeString    string `conffee:"cli=foo,env=bar"`
	SomeInteger   int
	SomeInteger8  int8
	SomeSubObject exampleSubObject
}

func main() {
	conffee.Debug = true
	variable := exampleConfiguration{
		SomeString:    "",
		SomeInteger:   0,
		SomeInteger8:  0,
		SomeSubObject: exampleSubObject{
			SomeSubValue: "",
		},
	}
	conffee.ReadConfiguration("./example-configuration.json", &variable, true, true)
	fmt.Println(fmt.Sprintf("%+v", variable))
}
