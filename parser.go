package conffee

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

var Debug = false

const TAG_KEY = "conffee"
const TAG_SEPARATOR = ","
const TAG_SETTING_SEPARATOR = "="
const TAG_ENV_SETTING_KEY = "env"
const TAG_CLI_SETTING_KEY = "cli"
const TAG_REQUIRED_SETTING_KEY = "required"


func printDebug(args ...interface{}) {
	if Debug {
		log.Println(args...)
	}
}

func getEnvironmentVariableName(str string) string {
	snake := regexp.MustCompile("(.)([A-Z][a-z]+)").ReplaceAllString(str, "${1}_${2}")
	snake  = regexp.MustCompile("([a-z0-9])([A-Z])").ReplaceAllString(snake, "${1}_${2}")
	return strings.ToUpper(snake)
}

func readConfiguration(configurationFileName string) ([]byte, error) {
	var file []byte
	var err error

	configurationFiles := []string{"./" + configurationFileName, "/etc/" + configurationFileName}
	for _, c := range configurationFiles {
		file, err = ioutil.ReadFile(c)
		if err == nil {
			break
		}
	}

	return file, err
}

func findSettingFromTagValue(val, settingName string) (string, bool, error) {
	parts := strings.Split(val, TAG_SEPARATOR)
	for _, part := range parts {
		t := strings.Split(part, TAG_SETTING_SEPARATOR)
		if len(t) != 2 {
			return "", false, errors.New("Invalid tag format: " + part)
		}
		if t[0] == settingName {
			return t[1], true, nil
		}
	}
	return "", false, nil
}

func defineCliArgument(field reflect.StructField, v reflect.Value, cliArgName string, required bool) {
	isRequired := "false"
	if required {
		isRequired = "true"
	}

	switch field.Type.Kind() {
	case reflect.String:
		if v.IsValid() {
			flag.String(cliArgName, "", cliArgName + " (string), required: " + isRequired)
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		if v.IsValid() {
			flag.Int64(cliArgName, 0, cliArgName + " (integer), required: " + isRequired)
		}
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		if v.IsValid() {
			flag.Uint64(cliArgName, 0, cliArgName + " (unsigned integer), required: " + isRequired)
		}
	case reflect.Float32, reflect.Float64:
		if v.IsValid() {
			flag.Float64(cliArgName, 0, cliArgName + " (float), required: " + isRequired)
		}
	case reflect.Bool:
		if v.IsValid() {
			flag.Bool(cliArgName, false, cliArgName + " (boolean), required: " + isRequired)
		}
	default:
		printDebug("Detected unknown/unsupported type: " + field.Type.String())
	}
}

func setVariable(field reflect.StructField, v reflect.Value, value string) bool {
	switch field.Type.Kind() {
	case reflect.String:
		if v.IsValid() {
			v.SetString(value)
			return true
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		if v.IsValid() {
			parsedValue, err := strconv.ParseInt(value, 10, 64)
			if err != nil {
				printDebug("Could not parse " + value + " to int")
			} else {
				v.SetInt(parsedValue)
				return true
			}
		}
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		if v.IsValid() {
			parsedValue, err := strconv.ParseUint(value, 10, 64)
			if err != nil {
				printDebug("Could not parse " + value + " to uint")
			} else {
				v.SetUint(parsedValue)
				return true
			}
		}
	case reflect.Float32, reflect.Float64:
		if v.IsValid() {
			parsedValue, err := strconv.ParseFloat(value, 64)
			if err != nil {
				printDebug("Could not parse " + value + " to float")
			} else {
				v.SetFloat(parsedValue)
				return true
			}
		}
	case reflect.Bool:
		if v.IsValid() {
			parsedValue, err := strconv.ParseBool(value)
			if err != nil {
				printDebug("Could not parse " + value + " to bool")
			} else {
				v.SetBool(parsedValue)
				return true
			}
		}
	default:
		printDebug("Detected unknown/unsupported type: " + field.Type.String())
	}
	return false
}


func setFromEnvironmentVariable(field reflect.StructField, v reflect.Value, envVarName string) bool {
	if envVar := os.Getenv(envVarName); envVar != "" {
		return setVariable(field, v, envVar)
	}
	return false
}


func defineAndParseCliArguments(target interface{}) error {
	// Go through the fields to create the command line options

	val := reflect.Indirect(reflect.ValueOf(target))
	valPtr := reflect.ValueOf(target)

	for i := 0; i < val.Type().NumField(); i++ {
		field := val.Type().Field(i)
		fieldName := field.Name
		v := valPtr.Elem().FieldByName(fieldName)

		cliArgName := fieldName
		required := false

		tagValue, tagFound := field.Tag.Lookup(TAG_KEY)

		if tagFound {
			cliSettingValue, cliSettingFound, err := findSettingFromTagValue(tagValue, TAG_CLI_SETTING_KEY)
			if err != nil {
				return err
			}
			if cliSettingFound {
				cliArgName = cliSettingValue
			}
			requiredSettingValue, requiredSettingFound, err := findSettingFromTagValue(tagValue, TAG_REQUIRED_SETTING_KEY)
			if err != nil {
				return err
			}
			if requiredSettingFound && strings.ToLower(requiredSettingValue) == "true" {
				required = true
			}
		}
		defineCliArgument(field, v, cliArgName, required)
	}
	flag.Parse()

	return nil
}


func parseConfiguration(fileContent []byte, target interface{}, parseEnvironment bool) error {
	val := reflect.Indirect(reflect.ValueOf(target))
	valPtr := reflect.ValueOf(target)
	if val.Kind() != reflect.Struct {
		return errors.New("Configuration target must be a struct, got " + val.Kind().String())
	}
	json.Unmarshal(fileContent, &target)

	parsedCliArguments := make(map[string]interface{})
	flag.Visit(func(f *flag.Flag) {
		parsedCliArguments[f.Name] = f.Value
	})

	for i := 0; i < val.Type().NumField(); i++ {
		field := val.Type().Field(i)
		fieldName := field.Name
		v := valPtr.Elem().FieldByName(fieldName)

		cliArgName := fieldName
		if parseEnvironment {
			envVarName := getEnvironmentVariableName(fieldName)
			//required := false

			tagValue, tagFound := field.Tag.Lookup(TAG_KEY)

			if tagFound {
				envSettingValue, envSettingFound, err := findSettingFromTagValue(tagValue, TAG_ENV_SETTING_KEY)
				if err != nil {
					return err
				}
				if envSettingFound {
					envVarName = envSettingValue
				}

				cliSettingValue, cliSettingFound, err := findSettingFromTagValue(tagValue, TAG_CLI_SETTING_KEY)
				if err != nil {
					return err
				}
				if cliSettingFound {
					cliArgName = cliSettingValue
				}

				//requiredSettingValue, requiredSettingFound, err := findSettingFromTagValue(tagValue, TAG_REQUIRED_SETTING_KEY)
				//if err != nil {
				//	return err
				//}
				//if requiredSettingFound && strings.ToLower(requiredSettingValue) == "true" {
				//	required = true
				//}
			}

			setFromEnvironmentVariable(field, v, envVarName)
		}
		cliArg, cliArgOk := parsedCliArguments[cliArgName]
		if cliArgOk {
			setVariable(field, v, fmt.Sprintf("%v", cliArg))
		}
	}
	return nil
}

func ReadConfiguration(configurationFileName string, target interface{}, requireConfigurationFile bool, parseEnvironment bool) error {
	if err := defineAndParseCliArguments(target); err != nil {
		return err
	}
	f, e := readConfiguration(configurationFileName)
	if e != nil {
		printDebug(fmt.Sprintf("File error: %v", e))
		if requireConfigurationFile {
			return e
		}
	}
	return parseConfiguration(f, target, parseEnvironment)
}
