package conffee

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"testing"
)

type TestConfigurationSimple struct {
	Field1 int
	Field2 string
}

var origEnv = os.Environ()

func Before(t *testing.T) {
	// Reset environment variables and commandline flags
	os.Clearenv()
	for _, pair := range origEnv {
		// From https://golang.org/src/os/env_test.go
		i := strings.Index(pair[1:], "=") + 1
		if err := os.Setenv(pair[:i], pair[i+1:]); err != nil {
			t.Errorf("Setenv(%q, %q) failed during reset: %v", pair[:i], pair[i+1:], err)
		}
	}
	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ExitOnError)
}

func Expected(expected, got interface{}, t *testing.T) {
	if expected != got {
		t.Error(fmt.Sprintf("Expected %v, got %v", expected, got))
	}
}

func TestDefineAndParseCliArguments_Basic(t *testing.T) {
	Before(t)
	conf := &TestConfigurationSimple{}
	os.Args = []string{"cmd", "-Field1", "2", "-Field2", "b"}
	err := defineAndParseCliArguments(conf)
	if err != nil {
		t.Fatal("Failed to parse command line arguments: ", err)
	}
}

func TestParseConfiguration_CheckFromCli(t *testing.T) {
	Before(t)

	conf := &TestConfigurationSimple{
		Field1: 1,
		Field2: "a",
	}
	os.Args = []string{"cmd", "-Field1", "2", "-Field2", "b"}
	defineAndParseCliArguments(conf)

	if err := parseConfiguration([]byte(""), conf, false); err != nil {
		t.Fatal("Failed to parse the configuration: ", err)
	}

	Expected(2, conf.Field1, t)
	Expected("b", conf.Field2, t)
}

func TestParseConfiguration_CheckFromEnv(t *testing.T) {
	Before(t)

	conf := &TestConfigurationSimple{
		Field1: 1,
		Field2: "a",
	}
	os.Args = []string{"cmd", "-Field1", "2"}
	os.Setenv("FIELD2", "c")
	defineAndParseCliArguments(conf)

	if err := parseConfiguration([]byte(""), conf, true); err != nil {
		t.Fatal("Failed to parse the configuration: ", err)
	}

	Expected(2, conf.Field1, t)
	Expected("c", conf.Field2, t)
}

func TestParseConfiguration_CheckCliAndEnvPrecedence(t *testing.T) {
	Before(t)

	conf := &TestConfigurationSimple{
		Field1: 1,
		Field2: "a",
	}
	os.Args = []string{"cmd", "-Field1", "2", "-Field2", "b"}
	os.Setenv("FIELD2", "c")
	defineAndParseCliArguments(conf)

	parseConfiguration([]byte(""), conf, true)

	Expected(2, conf.Field1, t)
	Expected("b", conf.Field2, t)
}

func TestParseConfiguration_FileContent(t *testing.T) {
	Before(t)

	conf := &TestConfigurationSimple{
		Field1: 1,
		Field2: "a",
	}
	os.Args = []string{"cmd", "-Field1", "2"}
	defineAndParseCliArguments(conf)
	fileContent := `
{
	"Field2": "b"
}
`
	parseConfiguration([]byte(fileContent), conf, true)
	Expected(2, conf.Field1, t)
	Expected("b", conf.Field2, t)
}

func TestParseConfiguration_FileContentAndEnvPrecedence(t *testing.T) {
	Before(t)

	conf := &TestConfigurationSimple{
		Field1: 1,
		Field2: "a",
	}
	os.Args = []string{"cmd", "-Field1", "2"}
	os.Setenv("FIELD2", "c")

	defineAndParseCliArguments(conf)
	fileContent := `
{
	"Field2": "b"
}
`
	parseConfiguration([]byte(fileContent), conf, true)
	Expected(2, conf.Field1, t)
	Expected("c", conf.Field2, t)
}

func TestParseConfiguration_FileContentCliAndEnvPrecedence(t *testing.T) {
	Before(t)

	conf := &TestConfigurationSimple{
		Field1: 1,
		Field2: "a",
	}
	os.Args = []string{"cmd", "-Field1", "2", "-Field2", "d"}
	os.Setenv("FIELD2", "c")

	defineAndParseCliArguments(conf)
	fileContent := `
{
	"Field2": "b"
}
`
	parseConfiguration([]byte(fileContent), conf, true)
	Expected(2, conf.Field1, t)
	Expected("d", conf.Field2, t)
}

func TestParseConfiguration_Help(t *testing.T) {
	Before(t)

	conf := &TestConfigurationSimple{}
	os.Args = []string{"cmd", "-h"}

	defineAndParseCliArguments(conf)
	t.Fail() // Test should not get here
}